package com.example.gregorynowik.testtechniquekinomap;

import android.util.Log;

import com.example.gregorynowik.testtechniquekinomap.model.VehicleListRoot;
import com.example.gregorynowik.testtechniquekinomap.network.RetrofitService;
import com.example.gregorynowik.testtechniquekinomap.network.VehicleService;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;


/**
 * Created by gregorynowik on 23/01/18.
 */

public class MainActivityPresenter {

    private OnFetchedVehiclesListener onFetchedVehiclesListener;

    public MainActivityPresenter(OnFetchedVehiclesListener onFetchedVehiclesListener) {
        this.onFetchedVehiclesListener = onFetchedVehiclesListener;
    }

    public void getVehicles() {

        VehicleService vehicleService = RetrofitService.sharedInstance().getRetrofit().create(VehicleService.class);

        Observable<VehicleListRoot> vehicleListObservable = vehicleService.getVehicleList();

         vehicleListObservable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<VehicleListRoot>() {

                    @Override
                    public void onNext(VehicleListRoot vehicleListRoot) {
                        Log.d("VEICLE CALL RES SIZE", Integer.toString(vehicleListRoot.getVehicleList().getVehicleList().size()));
                        onFetchedVehiclesListener.onFetchedVehicles(vehicleListRoot.getVehicleList().getVehicleList());
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("ERROR", e.getMessage());
                    }

                    @Override
                    public void onComplete() {

                    }
                });


    }


}
