package com.example.gregorynowik.testtechniquekinomap.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by gregorynowik on 23/01/18.
 */

public class Vehicle {

    private int id;
    private String name;
    @SerializedName("icon")
    private VehicleIcon vehicleIcon;

    public VehicleIcon getVehicleIcon() {
        return vehicleIcon;
    }

    public void setVehicleIcon(VehicleIcon vehicleIcon) {
        this.vehicleIcon = vehicleIcon;
    }

    public Vehicle() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
