package com.example.gregorynowik.testtechniquekinomap;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.gregorynowik.testtechniquekinomap.model.Vehicle;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements OnFetchedVehiclesListener, AdapterView.OnItemClickListener {

    private List<Vehicle> vehicleList = new ArrayList<>();
    private MainActivityPresenter presenter;
    private VehicleAdapter vehicleListViewAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        presenter = new MainActivityPresenter(this);

        ListView vehicleListView = findViewById(R.id.vehicle_list_view);
        vehicleListViewAdapter = new VehicleAdapter(this);

        vehicleListView.setAdapter(vehicleListViewAdapter);
        vehicleListView.setOnItemClickListener(this);
        presenter.getVehicles();

    }

    @Override
    public void onFetchedVehicles(List<Vehicle> vehicleList) {
        this.vehicleList = vehicleList;
        vehicleListViewAdapter.refresh(vehicleList);
        vehicleListViewAdapter.notifyDataSetChanged();
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

        Vehicle clickedVehicle = vehicleList.get(i);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this)
                .setTitle(clickedVehicle.getName())
                .setMessage("ID: "+ vehicleList.get(i).getId() + "\n" + "icon url : " + clickedVehicle.getVehicleIcon().getVehicleIconUrl().getLeft())
                .setCancelable(true)
                .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.cancel();
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }
}
