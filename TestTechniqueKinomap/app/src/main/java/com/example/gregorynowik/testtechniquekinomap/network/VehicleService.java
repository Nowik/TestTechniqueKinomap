package com.example.gregorynowik.testtechniquekinomap.network;

import com.example.gregorynowik.testtechniquekinomap.model.VehicleListRoot;


import io.reactivex.Observable;
import retrofit2.http.GET;

/**
 * Created by gregorynowik on 23/01/18.
 */

public interface VehicleService {



    @GET("vehicle/list?icon=1&lang=en-gb&forceStandard=1&outputFormat=json&appToken=f5FxX9Q08DQ9j31Hg5Jx8qHJDVeAWpyFAEwNFV6QF11Mjtl6pbKvMElkWuDk2aVL5MW5bhdQnVp0bIF22dr3hyDVKW9a9H7o32QfIXKOwIJXTCkzXAETCjB8")
    Observable<VehicleListRoot> getVehicleList();
}
