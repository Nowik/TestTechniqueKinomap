package com.example.gregorynowik.testtechniquekinomap.model;

/**
 * Created by gregorynowik on 23/01/18.
 */

public class VehicleIconUrl {

    private String left;
    private String right;

    public VehicleIconUrl() {
    }

    public String getLeft() {
        return left;
    }

    public void setLeft(String left) {
        this.left = left;
    }

    public String getRight() {
        return right;
    }

    public void setRight(String right) {
        this.right = right;
    }
}
