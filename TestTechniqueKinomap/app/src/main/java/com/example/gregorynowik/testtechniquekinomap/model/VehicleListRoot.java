package com.example.gregorynowik.testtechniquekinomap.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by gregorynowik on 23/01/18.
 */

public class VehicleListRoot {

    private VehicleList vehicleList;

    public VehicleListRoot() {
    }

    public VehicleList getVehicleList() {
        return vehicleList;
    }

    public void setVehicleList(VehicleList vehicleList) {
        this.vehicleList = vehicleList;
    }
}
