package com.example.gregorynowik.testtechniquekinomap;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.gregorynowik.testtechniquekinomap.model.Vehicle;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by gregorynowik on 23/01/18.
 */

public class VehicleAdapter extends BaseAdapter{

    private Context context;
    private List<Vehicle> vehicleList = new ArrayList<>();
    private LayoutInflater inflater;

    public VehicleAdapter(Context context) {
        this.context = context;
        this.inflater = LayoutInflater.from(context);
    }

    public void refresh(List<Vehicle> vehicleList){
        this.vehicleList = vehicleList;
        this.notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        Log.d("ADAPTER", "COUNT = " + Integer.toString(vehicleList.size()));
        return vehicleList.size();
    }

    @Override
    public Object getItem(int i) {
        return vehicleList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = inflater.
                    inflate(R.layout.vehicle_cell, viewGroup, false);
        }

        Vehicle vehicle = vehicleList.get(i);

        TextView vehicleNameTextview = view.findViewById(R.id.vehicle_name_textview);
        ImageView vehiclePictureImageView = view.findViewById(R.id.vehicle_imageview);

        vehicleNameTextview.setText(vehicle.getName());

        Glide.with(context).load(vehicle.getVehicleIcon().getVehicleIconUrl().getLeft()).into(vehiclePictureImageView);




        return view;
    }
}
