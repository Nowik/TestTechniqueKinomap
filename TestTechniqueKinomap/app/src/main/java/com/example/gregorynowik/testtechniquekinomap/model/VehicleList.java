package com.example.gregorynowik.testtechniquekinomap.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by gregorynowik on 23/01/18.
 */

public class VehicleList {

    @SerializedName("response")
    private List<Vehicle> vehicleList;

    public VehicleList() {
    }

    public List<Vehicle> getVehicleList() {
        return vehicleList;
    }

    public void setVehicleList(List<Vehicle> vehicleList) {
        this.vehicleList = vehicleList;
    }
}
