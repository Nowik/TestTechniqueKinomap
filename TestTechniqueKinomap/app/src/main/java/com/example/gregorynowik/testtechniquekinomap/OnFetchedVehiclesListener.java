package com.example.gregorynowik.testtechniquekinomap;

import com.example.gregorynowik.testtechniquekinomap.model.Vehicle;

import java.util.List;

/**
 * Created by gregorynowik on 23/01/18.
 */

public interface OnFetchedVehiclesListener {
    void onFetchedVehicles(List<Vehicle> vehicleList);
}
