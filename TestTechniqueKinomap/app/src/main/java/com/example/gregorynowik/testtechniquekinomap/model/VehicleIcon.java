package com.example.gregorynowik.testtechniquekinomap.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by gregorynowik on 23/01/18.
 */

public class VehicleIcon {

    @SerializedName("url")
    private VehicleIconUrl vehicleIconUrl;

    public VehicleIcon() {
    }

    public VehicleIconUrl getVehicleIconUrl() {
        return vehicleIconUrl;
    }

    public void setVehicleIconUrl(VehicleIconUrl vehicleIconUrl) {
        this.vehicleIconUrl = vehicleIconUrl;
    }
}
